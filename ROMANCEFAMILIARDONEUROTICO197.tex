\chapter{Romance familiar do neurótico}

No indivíduo adulto, a substituição da autoridade parental é dos
resultados mais necessários, mas também mais dolorosos, do
desenvolvimento. É~de todo necessário que ela se realize, e deve"-se
supor que todos os que chegaram a uma normalidade na idade adulta
realizaram"-na em certa medida. Sim, o desenvolvimento da sociedade
reside sobretudo nessa oposição entre ambas as gerações. Há, porém, uma
classe de neuróticos em cuja condição deve se reconhecer o fracasso na
realização dessa tarefa.

Para a criança pequena os pais são, antes de mais nada, a autoridade
única e fonte de todas as crenças. O~desejo mais intenso, e também o de
consequências mais sérias, é o de tornar"-se grande como o pai ou como a
mãe, que lhe são o elemento do mesmo sexo. Mas isso não pode persistir
ante o crescente desenvolvimento intelectual, uma vez que paulatinamente
a criança passa a conhecer as categorias a que pertencem seus pais.
Passa a conhecer outros pais, compara"-os com os seus, e com isso adquire
o direito de pôr em dúvida as qualidades únicas e incomparáveis que lhes
atribuíra. Pequenos acontecimentos na vida da criança, que lhe causam
descontentamento, são motivo para se criticar os pais, e, valendo"-se de
algum conhecimento obtido, num ou noutro aspecto valorizar outros pais,
em detrimento de seus próprios, dando continuidade a essa postura. Com
base na neuropsicologia sabemos que para isso vêm colaborar, entre
outras, as mais intensas moções de rivalidade sexual. O~objeto de tais
ensejos é manifestamente o sentimento de preterição. São frequentes os
casos em que a criança é negligenciada, ou pelo menos se sente
negligenciada, nos quais dá pela falta do pleno amor dos pais, e em
particular, mas sobretudo, lamenta ter de partilhá"-lo com outros irmãos.
A~sensação de que suas próprias inclinações não são de todo
correspondidas logo se faz ventilar na ideia, não raro recordada
conscientemente já desde a primeira infância, de que se é enteado ou
adotado. Muitas pessoas que não se tornaram neuróticas recordam"-se de
tais episódios bastante bem e com notável frequência, nos quais --- não
raro em consequência de alguma leitura --- foi dessa forma que
interpretaram e responderam ao comportamento hostil dos pais. Mas aqui
já se revela a influência do sexo, uma vez que o garoto demonstra
francamente maior tendência a moções inamistosas para com seu pai do que
em relação à mãe, inclinando"-se com tanto mais intensidade a se pôr
livre dele do que dela. Já nas meninas essa atividade fantasista tende a
se mostrar bem mais atenuada. Nas moções anímicas dos anos de infância
assim recordadas conscientemente encontramos o fator que nos possibilita
a compreensão do mito.\footnote{Cf. \versal{FREUD}: ``Hysterisch Phantasien und ihre Beziehung zur Bisexualität''
{[}\emph{Fantasias histéricas e sua relação com a bissexualidade}{]}, em
que há também remissões à literatura sobre esse tema.
{[}\emph{Gesammelte Werke}, vol. \versal{VII}.{]}} 

Raras vezes recordado de maneira consciente, mas quase sempre de modo
passível de ser comprovado pela psicanálise, assim é o estágio posterior
dessa alienação inicial com relação aos pais, o qual pode ser designado
como romance familiar dos neuróticos. É~essencialmente característica da
neurose, como de todo e qualquer dom superior, uma particularíssima
atividade de fantasia, que se manifesta em primeiro lugar nas
brincadeiras infantis, para então, com início aproximado na fase da
pré"-puberdade, apoderar"-se do tema das relações familiares. Exemplo
característico desse fantasismo particular é a conhecida atividade do
sonho diurno, que avança para muito além da puberdade. Uma observação
precisa desses sonhos diurnos nos ensina que eles servem à satisfação de
desejos, da retificação da vida, e conhecem sobretudo duas metas: a
erótica e a ambiciosa (por trás da qual oculta"-se muitas vezes a
erótica). Por volta da idade já mencionada, a fantasia da criança
ocupa"-se da tarefa de se livrar dos menosprezados pais, e de
substituí"-los, via de regra, por outros, de posição superior na escala
social. Nesse sentido se fazem valer os encontros casuais com vivências
efetivas (o primeiro contato com o senhor do castelo ou com o
proprietário de terras local, ou com os nobres, na cidade). Essas
vivências casuais despertam a inveja da criança, que encontra sua
expressão em uma fantasia pela qual ambos os pais são substituídos por
outros, de linhagem mais nobre. A~técnica de realização de tais
fantasias, que naturalmente são conscientes à época, depende da destreza
e do material que a criança tem à disposição. Trata"-se por isso de
realizá"-las com maior ou menos empenho, a fim de obter verossimilhança.
A~esse estágio se chega em época na qual a criança ainda não se
apercebeu dos condicionamentos sexuais de sua origem.

Soma"-se a isso o conhecimento das diferenciadas relações sexuais entre
pai e mãe; se a criança percebe que \emph{pater sempre incertus est},
enquanto a mãe é \emph{certissima}, com isso os romances familiares
experimentam curiosa limitação: ele se satisfaz precisamente com
enaltecer o pai, enquanto, sendo algo inalterável a linhagem da mãe, não
se deve continuar a pô"-la em dúvida. Esse segundo estágio (sexual) do
romance familiar também é conduzido por um segundo motivo, que não se
encontra no primeiro estágio (assexual). Com o conhecimento dos
processos sexuais surge a tendência a pintar as situações e relações
eróticas, e para tanto se põe como força pulsional o prazer, e a mãe,
objeto da mais elevada curiosidade sexual, é trazida à situação de
infidelidade secreta e de secretas relações amorosas. Assim, aquelas
fantasias, de certo modo assexuais, são trazidas à altura do
entendimento atual.

De resto, o motivo da vingança e da retaliação, que antes ficava em
primeiro plano, também aqui se revela. Essas crianças neuróticas
costumam ser punidas pelos pais a título de correção de maus
comportamentos sexuais, e deles se vingam mediante tais fantasias.

Caso bastante peculiar é o das crianças nascidas mais tarde, das quais o
fato mais digno de nota é que roubam a primazia de seus predecessores
por meio de imaginações desse tipo (bem ao modo das intrigas de que se
tem registro na história), e é frequente não pouparem esforços em
atribuir à mãe tantas relações amorosas quantos são seus concorrentes.
Uma variação interessante desse romance familiar é a que se tem quando o
herói fantasiador{} atribui legitimidade para si, ao mesmo tempo em
que elimina como ilegítimos seus outros irmãos. Com isso, um interesse
específico pode conduzir o romance familiar, que com suas múltiplas
facetas e sua aplicabilidade variada vem de encontro a toda sorte de
esforços. Assim, o pequeno fantasista pode suprimir a relação de
parentesco com uma irmã, por quem, por acaso, ele se sinta sexualmente
atraído.\footnote{\emph{Traumdeutung}, 8ª edição, p\,242 {[}\emph{Gesammelte Werke}, vol.
I\versal{I}/\versal{III}.{]}} 

Aquele que, tomado de horror, desviar os olhos ante essa corrupção da
índole infantil, querendo negar a possibilidade de tais coisas, a ele
deve se fazer observar que todas essas imaginações aparentemente hostis
na verdade não têm intenção tão maligna, e, sob uma roupagem mais leve,
comprovam a ternura original da criança por seus pais, que se fez
conservar. A~infidelidade e ingratidão é apenas aparente; pois quando se
perscruta em detalhes a mais frequente dentre essas fantasias
romanescas, que é a substituição de ambos os pais ou somente do pai por
indivíduos mais eminentes, descobre"-se que esses pais novos e de nobre
estirpe são munidos de traços provenientes dos pais reais e inferiores.
Sim, a tendência, de um modo geral, de substituir o pai real por um de
maior nobreza, nada mais é do que a expressão da nostalgia da criança
por um tempo que se perdeu e era feliz, em que seu pai lhe aparecia como
o mais valoroso e forte, e sua mãe como a mais amada e mais bela. A~criança se volta do pai, que ela agora conhece, para aquele em que ela
acreditou nos primeiros anos de sua infância, e a fantasia se revela
mera expressão do lamento por aquele tempo feliz ter se esvanecido.
Portanto, é de pleno direito que a superestimação dos primeiros anos da
infância torne a surgir nessas fantasias. Uma contribuição interessante
para esse tema resulta do estudo dos sonhos. É~de modo semelhante que a
interpretação dos sonhos ensina que mesmo nos anos subsequentes, quando
sonhamos com as figuras de imperador ou imperatriz, tais insignes
personagens significam pai e mãe. Assim, também no sonho do adulto
normal é mantida a supervalorização infantil dos pais.
