\chapter{O poeta e o fantasiar}

Os leigos como nós desde sempre se viram fortemente impelidos a saber de
onde essa notável personalidade, que é a do escritor, extrai seu
material --- algo no sentido da pergunta que aquele cardeal dirigiu a
Ariosto ---, e como ele disso se aproveita para provocar em nós
estimulações, das quais por nós mesmos talvez não fôssemos capazes. Em
compensação, nosso interesse só se faz amplificar pela circunstância
segundo a qual o próprio poeta, se por nós questionado, não nos dá
nenhuma informação, ou nenhuma que seja satisfatória, e de modo algum se
mostra perturbado pelo nosso conhecimento, já que a melhor compreensão
das condições de escolha do material poético e da essência da arte de
configuração poética em nada contribui para nos fazermos poetas.

Se ao menos pudéssemos descobrir em nós ou em nossos pares uma atividade
de algum modo aparentada ao fazer poesia! A própria investigação nos
faria esperar obter um primeiro esclarecimento sobre a produção poética.
Na verdade, para tanto vislumbramos uma perspectiva; --- são os próprios
poetas que gostam de reduzir a distância entre sua peculiaridade e a
essência humana em geral; com muita frequência nos garantem que em todo
homem se esconde um poeta e que o último poeta só morrerá com o último
homem.

Não será o caso de buscar os primeiros vestígios da atividade poética já
entre as crianças? Para elas crianças, a ocupação preferida, e a que
mais intensamente se dedicam, é o brincar. Talvez devêssemos dizer: toda
criança que brinca se porta como um poeta, uma vez que ela cria para si
o seu próprio mundo, ou, para dizer com mais precisão, transpõe as
coisas de seu mundo para uma nova ordem, que lhe agrada. Seria incorreto
pensar que a criança não leva este mundo a sério; ao contrário: leva tão
a sério a sua brincadeira, que nela se utiliza de grandes cargas de
afeto. O~contrário do jogo não é a seriedade, e sim a realidade. Apesar
de todo o investimento afetivo, a criança consegue muito bem diferenciar
o seu universo de brincadeiras e a realidade, e com seus objetos e
relações imaginados gosta de imitar as coisas palpáveis e visíveis do
mundo real. Essa imitação não se diferencia muito no ``brincar'' da
criança e no ``fantasiar''.

Ora, o poeta faz o mesmo que a criança que brinca: cria um mundo de
fantasia e o leva muito a sério; isto é, ele o provê de grande
investimento afetivo, ao mesmo tempo em que nitidamente o separa da
realidade. E~a linguagem conserva esse parentesco entre as brincadeiras
em que a criança atua e o produzir poesia\label{poesia}, uma vez que, em
referência às atuações, tem"-se pela palavra \emph{Spiele} {[}jogos{]} as
encenações do poeta que necessitam de apoio nos objetos palpáveis, estes
sendo passíveis de representação, como brincadeira ou jogo:
\emph{Lustspiel} {[}comédia{]}, \emph{Trauerspiel} {[}tragédia{]}, a
pessoa que as representa sendo designada \emph{Schauspieler} {[}ator{]}.
Mas também da irrealidade do universo poético resultam muitas
consequências importantes para a técnica artística, pois muito do que na
condição de real não poderia trazer satisfação pode fazê"-lo no jogo da
fantasia, e muitas estimulações em si dolorosas podem ser fonte de
prazer para o auditório e ouvinte do poeta.

Em virtude de outra relação nos demoraremos ainda um instante na
oposição entre realidade e jogo. Quando, já crescida, a criança deixou
de brincar, tendo animicamente se esforçado, no curso de décadas, para
compreender a realidade da vida com a seriedade necessária, um dia ela
pode cair numa disposição anímica que volte a suprimir a oposição entre
jogo e realidade. O~adulto pode recordar"-se da elevada seriedade com que
outrora exerceu seu jogo infantil e, ao equiparar suas ocupações
pretensamente sérias àquelas brincadeiras infantis, lança por terra o
fardo pesado: liberta"-se da opressão insuportável que a vida impõe e dá
a si o inefável prazer, conquista o ganho de prazer mais elevado do
humor.

Assim, o adulto deixa de brincar. Aparentemente ele renuncia ao ganho
pelo prazer, que extraía do jogo. Mas quem conhece a alma humana sabe
que quase não lhe haveria algo tão difícil como o renunciar ao prazer
que um dia conheceu. Na verdade não podemos renunciar a nada; o que
parece ser uma renúncia é na realidade uma formação substitutiva ou um
sucedâneo. Desse modo, também o adulto, quando cessa de brincar, nada
mais tem do que a imitação de objetos reais; em vez de jogar, agora ele
fantasia. Constrói castelos de vento, criando o que se pode chamar de
sonhar acordado. Creio que a maioria das pessoas, em momentos de sua
vida, constrói fantasias. É~um fato ao qual durante muito tempo não se
atentou, razão pela qual não se fez justiça a seu significado.

O fantasiar do homem é menos fácil de observar do que os jogos das
crianças. A~criança brinca sozinha ou constrói com outras um sistema
fechado para os propósitos do jogo, assim como não brinca para os
adultos como se estes fossem espectadores, tampouco esconde deles suas
brincadeiras. Mas o adulto se envergonha de seu fantasiar, oculta"-o dos
demais, cultiva"-o como o que lhe é mais íntimo, e via de regra
preferiria reconhecer seus defeitos a compartilhar suas fantasias. Por
isso mesmo pode"-se tomá"-lo como o único a construir tais fantasias, sem
sequer suspeitar da disseminação geral de criações bem semelhantes nas
outras pessoas. Esse comportamento diferenciado do que joga e do que
fantasia tem sua boa fundamentação nos motivos dessas atividades, uma
sendo continuação da outra.

O jogo da criança era direcionado por desejos, e era"-o efetivamente por
um desejo que auxilia em sua educação: o desejo de ser grande e adulto.
A~criança brinca sempre de ``ser grande'', imita no jogo o que veio a
conhecer na vida dos grandes. Ela não tem nenhum motivo para ocultar
esse desejo. Algo diferente se passa com o adulto; por um lado, o que se
espera dele é que já não jogue ou fantasie, mas que atue no mundo real;
por outro, entre os desejos que produzem o fantasiar há muitos que é
preciso esconder; eis por que ele se envergonha de seu fantasiar, como
sendo infantil e interdito.

Seria o caso de se perguntar como se têm informações tão precisas sobre
o fantasiar dos homens, uma vez que isso é tão sigilosamente encoberto.
Bem, há um gênero de pessoas que conferem não a um deus, mas a uma deusa
bastante severa --- a necessidade --- a missão de enunciar suas penas e
alegrias. São os neuróticos, que ao médico, de quem esperam o
restabelecimento pelo tratamento psíquico, devem confessar suas
fantasias. Dessa fonte provém o nosso melhor conhecimento, e chegamos
então à bem fundamentada conjectura segundo a qual os nossos doentes só
nos comunicam o que já poderíamos aprender com as pessoas saudáveis.

Então, passemos em revista algumas das características do fantasiar.
Poder"-se"-ia dizer que o feliz não fantasia nunca; só o faz o
insatisfeito. Desejos insatisfeitos são as forças pulsionais das
fantasias, e cada fantasia individual é uma satisfação de desejo, uma
correção da realidade insatisfatória. Os desejos impulsionantes são
diferentes a depender do sexo, do caráter e das relações de vida da
personalidade que fantasia. Facilmente podem ser agrupados segundo duas
correntes principais: ou são desejos ambiciosos, que servem à exaltação
da personalidade, ou são desejos eróticos. Nas mulheres jovens
predominam quase exclusivamente os desejos eróticos, pois sua ambição,
via de regra, é consumida por aspirações amorosas; nos homens jovens,
tão urgentes quanto os desejos eróticos são os de ordem egoísta e de
ambição. Mas não queremos acentuar a oposição entre ambas as
orientações, e sim a sua frequente unificação; assim como em muitas
imagens de altar, em um canto, pode haver o retrato de seu benfeitor, da
mesma forma podemos, nas fantasias de ambição, descobrir em algum canto
a dama para a qual aquele que fantasia realiza todos os seus atos
heroicos, e a cujos pés deposita todos os seus feitos exitosos. Veem
vocês que temos motivos suficientemente fortes para a ocultação; de um
modo geral, à mulher bem"-educada se concede um mínimo de necessidade
erótica, e o homem jovem deve aprender a sufocar uma desmesura de
sentimento de si, que ele traz dos mimos em que foi envolvido na
infância, para fins de adaptação a uma sociedade tão rica em indivíduos
com semelhantes pretensões.

Guardemo"-nos de representar como estáticos e imutáveis os produtos dessa
atividade fantasiadora, em suas fantasias individuais, castelos de vento
ou sonhos de vigília. Estes mais se acomodam às impressões de vida
mutáveis, alteram"-se com toda e qualquer oscilação das condições de
vida, acolhidos a cada impressão nova e operante de uma assim chamada
``marca temporal''. A~relação da fantasia com o tempo é de modo geral
muito significativa. Poder"-se"-ia dizer: uma fantasia como que paira em
três tempos, que são os três momentos temporais de nosso representar. O~trabalho anímico se atrela a uma impressão atual, a um ensejo que, neste
instante, tem condições de despertar um dos grandes desejos da pessoa, e
este remonta à lembrança de uma vivência anterior, o mais das vezes
infantil, na qual aquele desejo era satisfeito e produzia"-se uma
situação referente ao futuro, que se constituía como satisfação do
desejo, precisamente do sonho acordado ou da fantasia, que traz em si os
vestígios de sua origem enquanto motivo e lembrança. Portanto, o
passado, o presente e o futuro se enfileiram no cordão do desejo que os
percorre.

O mais banal dentre os exemplos pode lhes tornar mais clara a minha
exposição. Tome"-se o caso de um jovem pobre, órfão desamparado, ao qual
se passou o endereço de um empregador, que talvez lhe pudesse dar uma
colocação. Eis que no caminho o jovem se deixa sonhar acordado e imagina
algo adequado àquela situação. O~conteúdo da fantasia era algo como se
ele viesse a cair nas graças do novo chefe, se tornasse indispensável
nos negócios, fosse cooptado pela família de seu senhor, casasse com a
atraente filhinha da casa, para então se fazer coproprietário e, na
continuidade, passar à frente dos negócios. Com isso o jovem sonhador
substitui o que ele possuía na infância feliz: uma casa que o protegia,
pais amorosos e os primeiros objetos de suas inclinações afetivas. Nesse
exemplo, pode"-se ver como o desejo se utiliza de uma ocasião do momento
presente, esboçando para si, segundo o padrão do passado, uma imagem de
futuro.

Haveria ainda muito a se dizer sobre as fantasias, porém me limitarei às
alusões mais breves. O~caráter das fantasias, que sufocam e se tornam
irresistíveis, produz as condições para que se caia na neurose ou na
psicose. As fantasias são também os próximos estágios anímicos dos
sintomas de sofrimento, dos quais se queixam nossos pacientes. E~aqui
temos o afluir de um amplo atalho para a patologia.

No entanto, não posso ignorar a relação das fantasias com o sonho.
Também nossos sonhos noturnos nada mais são do que fantasias, que
podemos tornar evidentes pela interpretação dos sonhos. Em sua
insuperável sabedoria, a linguagem já há muito se decidiu pela essência
dos sonhos, fazendo chamar também de ``sonhos diurnos'' as criações no
ar feitas por quem fantasia. Se, apesar dessa indicação de sentido,
nossos sonhos continuam a parecer o mais das vezes obscuros, isso se
deve a uma única circunstância: tal como acontece à noite, são em nós
acionados os sonhos de que nos envergonhamos e que temos de ocultar de
nós mesmos, razão pela qual os recalcamos, empurrando"-os para o
inconsciente. A~esses desejos recalcados, bem como a seus rebentos, só
se pode permitir uma expressão gravemente desfigurada. Com a
bem"-sucedida elucidação da desfiguração do sonho pelo trabalho
científico, já não é difícil reconhecer que os sonhos noturnos, tal como
os sonhos diurnos --- estas fantasias bem conhecidas de todos nós ---,
são satisfações de desejos.

Basta de fantasias! Passemos agora ao poeta. Podemos realmente fazer a
tentativa de comparar o poeta com o ``sonhador à luz do dia'', e suas
criações com o sonhar acordado? De imediato se impõe uma primeira
diferenciação, entre os autores que criam com base em materiais prontos
e aceites, como os antigos épicos e trágicos, e aqueles que parecem
criá"-los livremente. Vamos nos deter nesses últimos, buscando para a
nossa comparação não exatamente aquele autor tido em alto conceito pela
crítica, mas sim o despretensioso narrador de romances, novelas e
histórias, que para isso encontra ávidos e numerosos leitores e
leitoras. Nas criações desses narradores um traço nos salta aos olhos:
todos têm um herói para o qual o autor procura, por todos os meios,
angariar simpatia, e ao qual ele parece proteger com uma providência
especial. Se, ao final do capítulo de um romance, deparo com o herói
inconsciente e sangrando em virtude de graves ferimentos, posso estar
certo de que, ao iniciar o capítulo seguinte, irei encontrá"-lo sob
zelosos cuidados e em vias de restabelecimento; e se o primeiro volume
acaba com o naufrágio do navio em uma tormenta, e nesse navio se
encontra o nosso herói, posso ter a certeza de que no início do segundo
volume lerei sobre o seu miraculoso salvamento, sem o que o romance não
teria continuidade. O~sentimento de segurança com que acompanho os
heróis em seu perigoso destino é bem aquele que leva o verdadeiro herói
a se lançar às águas para salvar quem está se afogando, ou se expor a
fogo inimigo para tomar de assalto uma bateria. É~o autêntico sentimento
do herói, ao qual um de nossos melhores autores dramaturgos,
Anzengruber, emprestou a deliciosa expressão: ``Nada lhe acontecerá''.
Mas penso que, junto a esses indícios traiçoeiros de invulnerabilidade,
pode"-se facilmente reconhecer Sua Majestade o eu, o herói de todo sonhar
acordado, bem como de todos os romances.

Ainda outros traços típicos dessas narrativas egocêntricas apontam para
igual parentesco. Se sempre acontece que todas as mulheres do romance se
apaixonam pelo herói, isso dificilmente pode ser apreendido como
descrição da realidade, mas facilmente pode ser compreendido como um
patrimônio necessário do sonhar acordado. O~mesmo se tem quando as
outras personagens do romance são nitidamente separadas em boas e más,
no que se renuncia à variedade de matizes das características humanas
que se observam na realidade; em relação ao eu tornado herói, os
``bons'' são necessariamente os que salvam, enquanto os maus são seus
inimigos e concorrentes.

De modo algum queremos ignorar que muitas criações poéticas em muito se
distanciam do modelo ingênuo do sonho acordado, mas não posso abafar a
conjectura de que também os desvios mais extremados podem ligar"-se a
esse modelo mediante uma série de transições contínuas. E~ainda, em
muitos dos chamados romances psicológicos, me chamou a atenção que
apenas uma pessoa, invariavelmente o herói, é descrita de dentro; em sua
alma, por assim dizer, se assenta o poeta, contemplando as outras
pessoas de fora. O~romance psicológico indubitavelmente deve muito de
sua peculiaridade à tendência do autor moderno, de cindir o eu, por
observação de si, em eus parciais e, em consequência disso, personificar
em vários heróis as correntes de conflito de sua vida anímica. Em
especialíssima oposição ao tipo de sonho diurno, aparecem os romances
que poderiam ser caracterizados como ``ex"-cêntricos'', nos quais a
personagem introduzida como herói desempenha o papel menos ativo, muito
mais como um espectador que vê passarem diante de si os feitos e
sofrimentos dos outros. Tem esse caráter a maior parte dos romances
tardios de Zola. No entanto, devo observar que a análise psicológica dos
indivíduos não poetas, que em tantos pontos se desviam da assim chamada
norma, tem nos ensinado sobre variações análogas do sonho diurno, em que
o eu se aparta do papel do espectador.

Para que se faça valer a nossa comparação do poeta com o que sonha
acordado, da criação poética com o sonho diurno, ela de algum modo deve
se mostrar profícua. Podemos tentar algo como empregar a tese
apresentada há pouco, da relação da fantasia com os três tempos e do
desejo que os perpassa no trabalho do escritor, e estudar, também com
sua ajuda, a relação entre a vida do escritor e suas criações. Via de
regra não se sabe com quais representações de expectativas é o caso de
abordar esse problema; frequente é representar essa relação de maneira
por demais simplificada. Valendo"-nos do exame obtido pelas fantasias,
devemos esperar pelo seguinte estado de coisas: uma vivência fortemente
atual desperta no poeta a lembrança de uma vivência anterior, o mais das
vezes pertencente à sua infância, que na poesia produz a sua satisfação;
a própria poesia permite reconhecer tanto os elementos da ocasião em
questão como os da antiga lembrança.

A complexidade dessa fórmula não deve assustá"-los; suponho que ao final
ela dará provas de ser um esquema por demais escasso, mas uma primeira
abordagem das circunstâncias reais poderia bem estar contida em tal
esquema, e após algumas tentativas, que empreendo aqui, sou levado a
pensar que esse modo de considerar as produções poéticas não deve ser
descartado como improfícuo. Não devemos esquecer que a ênfase talvez
surpreendente da lembrança infantil na vida do poeta se deduz, antes de
qualquer outra coisa, do pressuposto de que a poesia, assim como o sonho
de vigília, vem a ser uma continuação e um substitutivo da brincadeira
infantil de outrora\label{outrora}.

Não devemos deixar de recorrer àquela classe de poesias nas quais
vislumbramos não criações livres, e sim elaborações de materiais prontos
e conhecidos. Também nestas permanece no poeta um traço de autonomia,
que se deixa manifestar na escolha do material e em sua modificação, que
é frequentemente ampla. Mas, à medida que o material é dado, dele se
origina um tesouro popular de mitos, sagas e lendas. Ora, a indagação
dessas imagens em psicologia popular de modo algum é conclusa, mas,
citando como exemplo o que diz respeito aos mitos, é completamente
provável que eles correspondam a vestígios desfigurados de fantasias de
desejo de nações inteiras, de sonhos seculares da jovem humanidade.

Vocês poderão dizer que lhes falei muito mais de fantasias do que do
poeta, este que no entanto antepus no título de minha exposição. Sei
disso, e, pela menção do estado atual de nosso conhecimento, eu tentaria
justificar"-me. Eu poderia trazer até vocês apenas estímulos e
provocações, que se alastram do estudo das fantasias para o problema da
escolha de material poético. Ao outro problema, qual seja, com que
recursos o poeta provoca em nós impressões afetivas por meio de suas
criações, não fizemos qualquer alusão. Eu gostaria pelo menos de mostrar
a vocês qual via de nossas elucidações conduz das fantasias aos
problemas do efeito poético.

Vocês devem lembrar"-se de que, como dizíamos, aquele que sonha acordado
se esmera em ocultar dos outros suas fantasias, pois sente haver motivos
para se envergonhar delas. E~agora acrescento que, mesmo se ele as
partilhasse conosco, não nos proporcionaria nenhum prazer com tal
revelação. Quando experimentamos essas fantasias, sentimos aversão por
elas, ou mantemos uma frieza distante. Mas quando o poeta joga para nós
o seu jogo, como se fôssemos espectadores, ou quando relata, como seus
sonhos diurnos e pessoais, o que nós mesmos estaríamos inclinados a
expor, sentimos elevado prazer, cujas muitas fontes provavelmente se
confundem. Como o autor realiza tal coisa, eis aí o segredo que lhe é
mais particular. Na técnica de superação de tal repulsa, por certo
relacionada às barreiras que se erguem entre todo eu individual e os
outros, reside a verdadeira \emph{Ars poetica}. Podemos entrever duas
classes de técnica: o poeta atenua o caráter egoísta do sonho diurno
mediante modificações e encobrimentos e nos seduz por um ganho de prazer
puramente formal, ou seja, estético. Chamamos de prêmio de incentivo, ou
de prazer prévio, ao ganho de prazer que nos é oferecido para com ele
possibilitar o desprendimento de mais prazer de fontes psíquicas
profundas e copiosas. Sou da opinião de que todo prazer estético, que
nos é proporcionado pelo poeta, traz em si o caráter dessa perda, e que
o verdadeiro desfrute do trabalho do poeta advém da libertação de
tensões anímicas. Talvez em não pouca medida contribua para esse
resultado o fato de o poeta nos inserir em uma posição em que
desfrutamos de nossas próprias fantasias sem qualquer censura ou
vergonha. Aqui estaríamos às portas de investigações novas,
interessantes e complexas, mas, pelo menos desta vez, chegamos ao final
destas elucidações.
