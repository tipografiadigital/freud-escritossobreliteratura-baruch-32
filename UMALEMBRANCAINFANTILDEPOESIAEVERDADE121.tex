\chapter{Uma lembrança infantil de Poesia e verdade}

\emph{\emph{A~irmã que veio depois de Goethe, Cornelia Friederica
Christiana, nasceu em 7 de dezembro de 1750, quando ele contava quinze
meses de vida. Em razão dessa reduzida diferença de idade, está excluída
a possibilidade de ela ter sido objeto de ciúmes. Sabe"-se que as
crianças, no despertar de sua paixão, nunca desenvolvem reações tão
violentas contra os irmãos que já encontram, a sua aversão sendo
direcionada aos que vêm depois. Além disso a cena, de cuja interpretação
nos ocupamos, não é compatível com a tenra idade de Goethe no momento do
nascimento de Cornelia, nem mesmo pouco depois.}}

\emph{\emph{Por ocasião do nascimento do primeiro irmão varão, Hermann
Jakob, Johann Wolfgang tinha três anos e três meses. Cerca de dois anos
depois, contando aproximadamente cinco anos de idade, nasceu sua segunda
irmã. Uma e outra diferença de idade podem ser levadas em conta para a
datação da quebradeira das louças; é provável que se deva privilegiar o
primeiro; ele teria mais afinidade com o caso de meu paciente, que tinha
por volta de três anos e nove meses quando lhe nasceu o irmão. É~de
admirar que a autobiografia do irmão mais velho não traga nem uma
palavra sobre esse irmão.\footnote{\emph{{[}\emph{Adendo de 1924}:{]} Aproveito a oportunidade para
retificar uma afirmação incorreta, que não deveria ter sido emitida. Em
passagem posterior desse primeiro livro, o irmão mais novo não só é
mencionado, como é descrito. Isso quando se faz referência a uma penosa
enfermidade infantil, sob a qual também esse irmão ``não sofreu pouco''.
``Ele era de natureza terna, quieto e obstinado. Não chegamos a ter
propriamente uma relação, até porque ele mal sobreviveu aos anos da
infância.''}}  Ele teria cerca de seis anos
e Johann Wolfgang estava próximo dos dez. Sobre esse assunto, as notas
que me foram postas à disposição tecem as seguintes considerações:}}

\begin{quote}
\emph{\emph{\emph{Também o pequeno Goethe não se mostrou muito
ressentido com a morte de um irmãozinho. Ao menos é o que relata sua
mãe, segundo o testemunho de Bettina Brentano: ``A mãe achou estranho
que ante a morte de Jakob, o irmão mais novo, que era seu companheiro de
brincadeiras, ele não vertera uma lágrima sequer, e por sinal pareciam
incomodá"-lo os lamentos de seus pais e irmão; quando a mãe, algum tempo
depois, perguntou ao casmurro se ele não tinha amado a seu irmão, ele
correu para o seu quarto, puxou de sob a cama uma série de papéis onde
lições e histórias haviam sido escritas, e ele disse a ela que havia
feito tudo aquilo para ensinar ao irmãozinho''. O~irmão mais velho
sempre desempenhara o papel de pai em relação ao mais novo, e lhe
demonstrara a sua superioridade.}}}
\end{quote}
\emph{\emph{\emph{Portanto, podemos aqui constituir a opinião de que as
louças lançadas teriam sido uma ação simbólica, ou, de maneira mais
precisa: uma ação mágica, pela qual a criança (Goethe, tal como o meu
paciente) expressa de maneira vigorosa o seu desejo de afastamento do
intruso causador de incômodos. Não é preciso manifestar repulsa à
satisfação da criança com o despedaçamento dos objetos; se uma ação traz
prazer por si mesma, isso não é nenhum impedimento, mas antes uma
tentação para que ela a repita a serviço de outros propósitos. Mas não
acreditamos que o prazer em fazer tilintar e quebrar possa garantir a
essas traquinagens infantis um lugar duradouro na lembrança do adulto.
Tampouco nos opomos a complicar a motivação da ação adicionando ainda
outro elemento. A~criança, que espatifa as louças, está ciente de que
está fazendo algo ruim, pelo qual os adultos irão repreendê"-la; se ela
não se deixa deter ao saber disso, provavelmente está se comprazendo em
um sentimento de animosidade para com os pais: ela quer mostrar"-se
má.}}}

\emph{\emph{\emph{O~prazer do quebrar e pelo quebrado também seria
satisfeito se a criança se limitasse a lançar os frágeis objetos ao
chão. A única explicação estaria no ``lançar fora'', para a rua, pela
janela. Mas este ``para fora'' parece uma peça essencial da ação mágica
e deriva de seu sentido oculto. É~preciso desfazer"-se da nova criança,
possivelmente pela janela, pois pela janela foi que ela chegou. Toda a
ação equivaleria então àquela reação verbal de uma criança, que
conhecemos bem, quando se conta a ela que a cegonha trouxe um
irmãozinho. ``Leva de volta!'', é a resposta.}}}

\emph{\emph{\emph{Entretanto, não vamos querer negar quão delicado é ---
a prescindir de todas as incertezas internas --- fundamentar a
interpretação de uma ação infantil em uma única analogia. Por isso,
durante anos me detivera em minha concepção da pequena cena de
\emph{Poesia e verdade}. E~aconteceu de um dia eu receber um paciente
que iniciou a sua análise com as frases a seguir, as quais retive
textualmente:}}}

\begin{quote}
\emph{\emph{\emph{Sou o mais velho de oito ou nove
irmãos.\footnote{\emph{Ainda que se deva a uma distração, o erro aqui é digno de nota.
Dificilmente se pode rechaçar que ele teria sido induzido a ela já por
sua tendência a eliminar o irmão. {[}Cf. Ferenczi, \emph{Über passagere
Symptombildungen während der Analyse}. Zentralbl. f. Psychoanalyse \versal{II},
1912.{]}}}  Uma de minhas primeiras lembranças é de meu
pai, sentado de pijamas em sua cama, contando"-me sorrindo que eu ia
ganhar um irmãozinho. Eu tinha na época três anos e nove meses; tão
grande é a diferença de idade entre mim e o irmão que vem depois de mim.
Então eu sei que, pouco tempo depois disso (ou teria sido um ano
antes?),\footnote{\emph{O~próprio paciente voltou atrás e suprimiu essa dúvida, que
corroía, ao modo de resistência, o ponto essencial de sua confidência.}}  joguei pela janela uma série de objetos,
escovas --- ou seria apenas uma escova? --- sapatos e outros objetos.
Tenho também uma lembrança anterior a essa. Eu estava com dois anos e
passei a noite com meus pais em um quarto de hotel em Linz, em viagem
para Salzkammergut. Durante aquela noite estive de tal maneira inquieto
e fiz tamanha balbúrdia, que meu pai teve de me bater.}}}
\end{quote}
\emph{\emph{\emph{Com essa declaração, eu abro mão de toda e qualquer
dúvida. Quando na atitude analítica duas coisas seguem imediatamente uma
à outra, como que trazidas em um só sopro, devemos converter essa
semelhança em conexão. Assim foi, como se o paciente tivesse dito:
``Porque eu fiquei sabendo que ia ter um irmão, algum tempo depois
joguei aquelas coisas na rua''. O~lançar fora de escovas, sapatos etc.
se dá a reconhecer como reação ao nascimento do irmão. É~digno de
apreciação que os objetos arremessados nesse caso não eram louças, mas
outras coisas, provavelmente as que podiam ser alcançadas pela
criança… O ato de jogar fora (pela janela para a rua)
evidencia"-se assim como algo essencial à ação, o prazer de quebrar, de
fazer tilintar --- o tipo de coisas em que ``a execução é realizada'',
inconstante e inessencial.}}}

\emph{\emph{\emph{Naturalmente, a exigência da execução vale também para
a terceira lembrança infantil do paciente, que, mesmo sendo a mais
primeva, é impelida para o fim da série. Trata"-se de uma exigência fácil
de cumprir. Nós entendemos que a criança de dois anos estava inquieta
por não conseguir suportar o pai e a mãe juntos na mesma cama. É~claro
que durante a viagem não se poderia evitar que a criança se convertesse
em testemunha dessa união. Dos sentimentos, que então se agitavam no
pequeno ciumento, permaneceu o ressentimento contra a mulher, que teve
como consequência um duradouro distúrbio em seu desenvolvimento
afetivo.}}}

\emph{\emph{\emph{Quando, após manifestar ambas as experiências no
círculo da Sociedade Psicanalítica {[}de Viena{]}, aventei que
ocorrências desse gênero não seriam raras entre as crianças pequenas,
pus à disposição da sra. dra. Von Hug"-Hellmuth as duas observações que
apresento a seguir:}}}

\section{I}

\emph{\emph{\emph{Com cerca de três anos e meio, o pequeno Erich ``de
repente'' adquiriu o hábito de jogar pela janela tudo de que não
gostasse. Mas passou a fazer isso até com objetos que não o estorvavam
nem lhe diziam respeito. Foi bem no dia do aniversário do pai --- quando
ele tinha três anos e quatro meses e meio --- que ele lançou um pesado
rolo de macarrão (sorrateiramente o havia arrastado da cozinha para o
quarto) por uma janela do terceiro pavimento da casa. Dias depois se
seguiram um morteiro, e então um par de pesadas botas de
montanhismo\footnote{\emph{Ele sempre escolhia objetos mais pesados.}}  do pai, que ele primeiro teve de tirar da
caixa.}}}

\emph{\emph{\emph{Então a mãe, no sétimo ou oitavo mês de gravidez, teve
um \emph{fausse couche}, e nisso a criança se mostrou ``como que
alternadamente comportada e ternamente quieta''. No quinto ou sexto mês
dizia repetidas vezes à mãe: ``Mamãe, eu pulo na sua barriga'' ou
``Mamãe, aperto a sua barriga''. E~pouco antes do \emph{fausse couche},
em outubro: ``Já que eu tenho de ter um irmãozinho, que pelo menos seja
depois do Natal''.}}}

\section{\versal{II}}

\emph{\emph{Uma jovem senhora de dezenove anos trouxe espontaneamente
como a mais remota lembrança infantil:}}

\begin{quote}
\emph{\emph{Eu me vejo terrivelmente malcriada, a ponto de sair me
arrastando pelo chão e ficar sentada sob a mesa da sala de jantar. Sobre
a mesa está a minha xícara de café --- ainda tenho nitidamente diante
dos olhos os desenhos da porcelana \mbox{---,} e lembro"-me de querer jogá"-la
pela janela, bem no instante em que vovó entrava no quarto.}}
\end{quote}
\begin{quote}
\emph{\emph{Ninguém estava se importando comigo, e nesse meio tempo no
café se formou uma ``pele'', que sempre me deu medo; até hoje dá.}}
\end{quote}
\begin{quote}
\emph{\emph{Nesse dia nascia o meu irmão, dois anos e meio mais novo do
que eu. Por isso ninguém tinha tempo para mim.}}
\end{quote}
\begin{quote}
\emph{\emph{Sempre me contam que nesse dia eu estava insuportável; no
almoço eu jogara para fora da mesa o copo preferido de papai; durante o
dia várias vezes eu sujei minha roupinha, e de manhã até a noite o meu
humor foi o pior possível. De tanta raiva, até a minha bonequinha de
banho eu acabei destruindo.}}
\end{quote}
\emph{\emph{Um e outro caso praticamente dispensam comentários. Sem
maiores esforços analíticos, eles confirmam que o ressentimento da
criança ante a aparição esperada ou já consumada de um concorrente se
expressa no arremessar de objetos pela janela como também por outros
atos de maldade e de compulsões destrutivas. Na primeira observação, os
``objetos ruins'' simbolizam precisamente a própria mãe, para a qual se
direciona a ira da criança, uma vez que o irmão ainda não está lá. O~garoto de três anos e meio sabe da gravidez da mãe e não tem dúvida de
que ela aloja uma criança em seu ventre. Com isso pode"-se lembrar do
``pequeno Hans''\footnote{\emph{\emph{Analyse der Phobie eines fünfjährigen Knaben.}
{[}\emph{Gesammelte Werke}, Bd. \versal{VII}.{]}}}  e do medo, que lhe era peculiar, de
carroças pesadamente carregadas.\footnote{\emph{Já faz algum tempo, uma senhora de seus cinquenta e poucos anos
produziu em mim mais uma confirmação para essa simbologia da gravidez.
Haviam lhe contado repetidas vezes que ela, quando criança, ainda mal
sabia falar, chamava o pai à janela sempre que pela rua passasse um
pesado caminhão de mudanças. Levando em conta as lembranças das casas em
que morou, pode"-se afirmar que na época ela teria não mais do que dois
anos e nove meses. Foi quando nasceu o irmão que veio imediatamente
depois dela. Em consequência desse aumento de família, seria necessário
mudar de casa. Mais ou menos ao mesmo tempo, era frequente que antes de
dormir ela tivesse a angustiante sensação de algo sinistramente grande,
que vinha a seu encontro, e então ``suas mãos {[}as dela{]} ficavam
enormes''.}}  Na segunda
observação, é interessante notar a idade da menina: dois anos e meio.}}

\emph{\emph{Se agora retornamos à lembrança infantil de Goethe e a
substituímos, na passagem que a refere em \emph{Poesia e verdade},
segundo o que acreditamos ter decifrado pela observação das outras
crianças, produz"-se uma conexão de pensamentos a que não se pode objetar
e que de outro modo não teríamos descoberto. Seria a seguinte: ``Eu fui
uma criança feliz; o destino me conservou com vida, ainda que eu tivesse
vindo ao mundo como um morto. O~mesmo destino eliminou meu irmão, de
modo que eu não precisei dividir o amor de minha mãe com ele''. E~então
prossegue a via do pensamento para outra morte daquelas épocas remotas:
a avó, que habitava outra parte da casa como um espírito amistoso e
tranquilo.}}

\emph{\emph{Em outra passagem expressei"-me da seguinte forma: quando se
foi o dileto indiscutível da mãe, por toda a vida conservará aquele
sentimento de conquistador, aquela confiança no êxito, que não raras
vezes realmente impele para o êxito. E~uma observação do seguinte
gênero: ``minha força tem suas raízes na relação com a mãe'' --- assim
teria Goethe, com todo o direito, iniciado a sua autobiografia.}}


